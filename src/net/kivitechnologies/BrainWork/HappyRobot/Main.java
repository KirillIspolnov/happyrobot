package net.kivitechnologies.BrainWork.HappyRobot;

/**
 * Класс, демонстрирующий работу робота
 * 
 * @author Испольнов Кирилл, 16ит18К
 */
public class Main {

    public static void main(String[] args) {
        System.out.println("Познакомьтесь с ВАЛЛ-И (Robot)!");
        //Создаем Вселенский Аннигилятор Ландшафтный Легкий, Интеллектуальный
        Robot walle = new Robot();
        walle.move(19, 5);
        
        System.out.println("Познакомьтесь с R2D2 (Droid)!");
        //R2D2 поможет Оби-Вану Кеноби спасти Палпатина 
        Droid r2d2 = new Droid();
        r2d2.move(15, 11);
    }

}
