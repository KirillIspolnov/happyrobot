package net.kivitechnologies.BrainWork.HappyRobot;

/**
 * Класс, представляющий робота
 * Этот робот может двигаться по одному шагу вперед в направлении, описываемом интерфесом Orientation
 * 
 * @author Испольнов Кирилл, 16ит18К
 */
public class Droid {
    private static final String START_MOVING_TO_FORMAT        = "Начианю движение в точку (%d, %d)%n";
    private static final String ROBOT_NOW_AT_FORMAT           = "Робот теперь находится здесь: (%d, %d)%n";
    private static final String ROBOT_WAS_AT_FORMAT           = "Робот находился здесь: (%d, %d)%n";

    /**
     * Интерфейс, позволяющий управлять направлением движения робота
     * 
     * @author Испольнов Кирилл, 16ит18К
     */
    public static interface Orientation {
        /**
         * Возвращает массив из двух координат робота 
         *  
         * @param x текущая х-координата робота 
         * @param y текущая у-координата робота
         * @return массив из двух новых координат робота
         */
        public int[] forward(int x, int y);
    }
    
    /* Константы предустановленных направлений движения робота */
    public static final Orientation NORTH, EAST, SOUTH, WEST;
    
    static {
        NORTH = new Orientation(){
            @Override
            public int[] forward(int x, int y) {
                return new int[]{ x, ++y };
            }
            
            @Override
            public String toString() {
                return "на север";
            }
        };
        EAST = new Orientation(){
            @Override
            public int[] forward(int x, int y) {
                return new int[]{ ++x, y };
            }
            
            @Override
            public String toString() {
                return "на восток";
            }
        };
        SOUTH = new Orientation(){
            @Override
            public int[] forward(int x, int y) {
                return new int[]{ x, --y };
            }
            
            @Override
            public String toString() {
                return "на юг";
            }
        };
        WEST = new Orientation(){
            @Override
            public int[] forward(int x, int y) {
                return new int[]{ --x, y };
            }
            
            @Override
            public String toString() {
                return "на запад";
            }
        };
    }
    
    //Текущаее направление движения
    private Orientation myOrientation;
    //Текущие координаты робота
    private int x, y;
    
    /**
     * Конструктор создающий робота, стоящего в начале координат
     * 
     * @param defaultOrientation направление движения
     */
    public Droid(Orientation defaultOrientation) {
        myOrientation = defaultOrientation;
        x = 0;
        y = 0;
    }
    
    /**
     * Конструктор создающий робота, стоящего в начале координат и имеющего северное направление движения
     */
    public Droid() {
        this(NORTH);
    }
    
    /**
     * Делает шаг вперед
     * 
     * @param printResults нужно ли выводить исходные и конечные координаты 
     */
    public void moveForward(boolean printResults) {
        if(printResults)
            System.out.printf(ROBOT_WAS_AT_FORMAT, x, y);
        
        int[] updated = myOrientation.forward(x, y);
        
        x = updated[0];
        y = updated[1];
        
        if(printResults)
            System.out.printf(ROBOT_NOW_AT_FORMAT, x, y);
    }
    
    /**
     * Делает шаг вперед и выводит координаты робота
     */
    public void moveForward(){
        moveForward(true);
    }
    
    /**
     * Проводит робота к заданной точке
     * 
     * @param x1 координата конечной точки на оси абсцисс 
     * @param y1 координата конечной точки на оси ординат
     */
    public void move(int x1, int y1) {
        System.out.printf(START_MOVING_TO_FORMAT, x, y);
        
        if(x < x1)
            rotateTo(EAST);
        else if(x > x1)
            rotateTo(WEST);
        
        while(x != x1) {
            moveForward();
        }
        
        if(y < y1)
            rotateTo(NORTH);
        else if(y > y1)
            rotateTo(SOUTH);
        
        while(y != y1) {
            moveForward();
        }
    }
    
    /**
     * Изменяет направление движения робота
     * 
     * @param printResults нужно ли выводить исходное и конечное направления движения 
     */
    public void rotateTo(Orientation newOrientation, boolean printResults) {
        if(printResults)
            System.out.println("Робот двигался " + myOrientation.toString());
            
        myOrientation = newOrientation;
        
        if(printResults)
            System.out.println("Робот теперь будет двигаться " + myOrientation.toString());        
    }
    
    /**
     * Изменяет направление движения робота и выводит направления движения робота
     */
    public void rotateTo(Orientation newOrientation) {
        rotateTo(newOrientation, true);
    }
    
    @Override
    public String toString() {
        return "Робот находится по координатам (" + x + ", " + y + "). Направление его движения - " + myOrientation.toString();
    }
}
