package net.kivitechnologies.BrainWork.HappyRobot;

/**
 * Класс, представляющий робота
 * Этот робот может двигаться по одному шагу вперед в направлении, описываемом интерфесом Orientation
 * 
 * @author Испольнов Кирилл, 16ит18К
 */
public class Robot {
    private static final String ROBOT_INFO_FORMAT             = "Робот находится по координатам (%d, %d). Направление его движения - \"%s\"";
    private static final String DIRECTION_OF_ROBOT_NOW_FORMAT = "Робот теперь будет двигаться по направлению \"%s\"%n";
    private static final String DIRECTION_OF_ROBOT_WAS_FORMAT = "Робот двигался по направлению \"%s\"%n";
    private static final String START_MOVING_TO_FORMAT        = "Начианю движение в точку (%d, %d)%n";
    private static final String ROBOT_NOW_AT_FORMAT           = "Робот теперь находится здесь: (%d, %d)%n";
    private static final String INCORRECT_DIRECTION_ERROR     = "Что-то я не понимаю направления своего движения...";
    private static final String ROBOT_WAS_AT_FORMAT           = "Робот находился здесь: (%d, %d)%n";

    /**
     * Перечисление констант, позволяющих управлять направлением движения робота
     * 
     * @author Испольнов Кирилл, 16ит18К
     */
    public static enum Direction {
        NORTH, EAST, SOUTH, WEST
    };
    
    //Текущаее направление движения
    private Direction direction;
    //Текущие координаты робота
    private int x, y;
    
    /**
     * Конструктор создающий робота, стоящего в начале координат
     * 
     * @param defaultOrientation направление движения
     */
    public Robot(Direction direction) {
        this.direction = direction;
        x = 0;
        y = 0;
    }
    
    /**
     * Конструктор создающий робота, стоящего в начале координат и имеющего северное направление движения
     */
    public Robot() {
        this(Direction.NORTH);
    }
    
    /**
     * Делает шаг вперед
     * 
     * @param printResults нужно ли выводить исходные и конечные координаты 
     */
    public void moveForward(boolean printResults) {
        if(printResults)
            System.out.printf(ROBOT_WAS_AT_FORMAT, x, y);
        
        switch(direction) {
            case NORTH: y++; break;
            case SOUTH: y--; break;
            case EAST: x++; break;
            case WEST: x--; break;
            default:System.out.println(INCORRECT_DIRECTION_ERROR);
        }
        
        if(printResults)
            System.out.printf(ROBOT_NOW_AT_FORMAT, x, y);
    }
    
    /**
     * Делает шаг вперед и выводит координаты робота
     */
    public void moveForward(){
        moveForward(true);
    }
    
    /**
     * Проводит робота к заданной точке
     * 
     * @param x1 координата конечной точки на оси абсцисс 
     * @param y1 координата конечной точки на оси ординат
     */
    public void move(int x1, int y1) {
        System.out.printf(START_MOVING_TO_FORMAT, x, y);
        
        if(x < x1)
            rotateTo(Direction.EAST);
        else if(x > x1)
            rotateTo(Direction.WEST);
        
        while(x != x1) {
            moveForward();
        }
        
        if(y < y1)
            rotateTo(Direction.NORTH);
        else if(y > y1)
            rotateTo(Direction.SOUTH);
        
        while(y != y1) {
            moveForward();
        }
    }
    
    /**
     * Изменяет направление движения робота
     * 
     * @param printResults нужно ли выводить исходное и конечное направления движения 
     */
    public void rotateTo(Direction direction, boolean printResults) {
        if(printResults)
            System.out.printf(DIRECTION_OF_ROBOT_WAS_FORMAT, this.direction.toString());
            
        this.direction = direction;
        
        if(printResults)
            System.out.printf(DIRECTION_OF_ROBOT_NOW_FORMAT, direction.toString());    
    }
    
    /**
     * Изменяет направление движения робота и выводит направления движения робота
     */
    public void rotateTo(Direction direction) {
        rotateTo(direction, true);
    }
    
    /**
     * Поворачивает робота на 90 градусов ПО ЧАСОВОЙ СТРЕЛКЕ
     */
    public void rotateLeft() {
        switch(direction) {
            case NORTH: rotateTo(Direction.WEST); break;
            case WEST: rotateTo(Direction.SOUTH); break;
            case SOUTH: rotateTo(Direction.EAST); break;
            case EAST: rotateTo(Direction.NORTH); break;
            default: System.out.println(INCORRECT_DIRECTION_ERROR);
        }
    }
    
    /**
     * Поворачивает робота на 90 градусов ПРОТИВ ЧАСОВОЙ СТРЕЛКИ
     */
    public void rotateRight() {
        switch(direction) {
            case NORTH: rotateTo(Direction.EAST); break;
            case EAST: rotateTo(Direction.SOUTH); break;
            case SOUTH: rotateTo(Direction.WEST); break;
            case WEST: rotateTo(Direction.NORTH); break;
            default: System.out.println(INCORRECT_DIRECTION_ERROR);
        }
    }
     
    @Override
    public String toString() {
        return String.format(ROBOT_INFO_FORMAT, x, y, direction.toString()); 
    }
}
